﻿using System;
using System.Threading;
using CreditCards.UITests.PageObjectModels;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Xunit;

namespace CreditCards.UITests
{
    public class CreditCardApplicationTests :IDisposable
    {
		private readonly IWebDriver _driver;
		private readonly ApplicationPage _applicationpage;
		const string APPLICATION_PAGE_TITLE = "Credit Card Application - CreditCards";
		const string APPLICATION_COMPLETE_TITLE = "Application Complete - CreditCards";
		const string FIRST_ERROR = "Please provide a first name";
		const string DECISION = "AutoDeclined";
		const string PAGE_PATH = "http://localhost:44108";

		public CreditCardApplicationTests()
		{
			_driver = new ChromeDriver();
			_driver.Url = PAGE_PATH;
			_applicationpage = new ApplicationPage(_driver);
			_applicationpage.NavigateTo();
		}

		public void Dispose()
		{
			_driver.Quit();
			_driver.Dispose();
		}

		[Fact]
		public void ShouldLoadApplicationPage_SmokeTest()
		{
			Assert.Equal(APPLICATION_PAGE_TITLE, _applicationpage.Driver.Title);
		}

		[Fact]
		public void ShouldValidateAplicationDetails()
		{
			FillOutFormFields();
			Assert.Equal(APPLICATION_PAGE_TITLE, _applicationpage.Driver.Title);
			Assert.Equal(FIRST_ERROR, _applicationpage.FirstErrorMessage);
		}

		[Fact]
		public void ShouldDeclineLowIncomes()
		{
			ApplicationCompletePage _completePage = FillOutFormFields(true);

			DelayForDemoVideo();

			Assert.Equal(APPLICATION_COMPLETE_TITLE, _completePage.Driver.Title);
			Assert.Equal(DECISION, _completePage.ApplicationDecision);
		}

		private ApplicationCompletePage FillOutFormFields(bool fillFirstName=false)
		{
			var firstName = fillFirstName ? "Sarah" : string.Empty;
			_applicationpage.EnterName(firstName, "Smith");
			DelayForDemoVideo();

			_applicationpage.EnterFrequentFlyerNumber("012345-A");
			DelayForDemoVideo();

			_applicationpage.EnterAge("35");
			DelayForDemoVideo();

			_applicationpage.EnterGrossAnnualIncome("10000");
			DelayForDemoVideo();

			 return _applicationpage.SubmitApplication();
		}

		private void DelayForDemoVideo()
		{
			Thread.Sleep(1000);
		}
	}
}
